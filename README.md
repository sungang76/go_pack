# go_pack

Experiment on export this repo package for others to use


# support for multiple module for singla repo
https://go.dev/doc/modules/managing-source#:~:text=You%20can%20publish%20multiple%20modules,must%20have%20its%20own%20go.

## Choosing repository scope
You publish code in a module when the code should be versioned independently from code in other modules.
<p>

Designing your repository so that it hosts a single module at its root directory will help keep maintenance simpler, particularly over time as you publish new minor and patch versions, branch into new major versions, and so on. However, if your needs require it, you can instead maintain a collection of modules in a single repository.

## Sourcing one module per repository
You can maintain a repository that has a single module’s source in it. In this model, you place your go.mod file at the repository root, with package subdirectories containing Go source beneath.

<p>
This is the simplest approach, making your module likely easier to manage over time. It helps you avoid the need to prefix a module version number with a directory path.


## Pakcage structure
If the quote package exists, the directory structure should look something like this:


```css
go_pack/
├── go.mod
├── quote/
│   └── quote.go
└── other_packages/
    └── ...
```


## Publish 

Make the module available by running the go list command to prompt Go to update its index of modules with information about the module you’re publishing.

<p>

Precede the command with a statement to set the GOPROXY environment variable to a Go proxy. This will ensure that your request reaches the proxy.

```bash
$ GOPROXY=proxy.golang.org go list -m example.com/mymodule@v0.1.0
```