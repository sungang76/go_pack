package mypackage2

import (
	"regexp"
	"testing"
)

func TestHelloName(t *testing.T) {
	name := "Gordon"
	want := regexp.MustCompile(`\b` + name + `\b`)
	msg, err := Hello("Gordon")
	if !want.MatchString(msg) || err != nil {
		t.Fatalf(`Hello("Gordon") = %q, %v, want match %#q, nil`, msg, err, want)
	}
}

// TestHelloEmpty calls greetings.Hello with an empty string,
// checking for an error.
func TestHelloEmpty(t *testing.T) {
	msg, err := Hello("")
	if msg != "" || err == nil {
		t.Fatalf(`Hello("") = %q, %v, want "", error`, msg, err)
	}
}
